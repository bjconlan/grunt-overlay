/**
 * Overlay Grunt multitask.
 * @see The <a href="http://gruntjs.com">Grunt</a> build system for more details.
 *
 * @author Benjamin Conlan
 * @since 0.1.0
 */
module.exports = function(grunt) {
  var path = require('path'),
      fs = require('fs');

  /**
   * This task provides some simple file copying operations as a grunt task.
   *
   * The expected gruntjs. configration fragment is expected to be formed as follows:
   *
   * ...
   * overlay: {                            // task
   *   js: {                               // profile (multitask)
   *     src: ['src/js/*.js', 'lib/*.js'], // source file search path
   *     dest: 'dest/js',                  // destination root path
   *     options: {
   *       overwrite: true,                // overwrite the file if it already exists
   *       base_path: 'src'                // excluded base path (in this case /src/js/** -> dest/js/js/**)
   *     },
   *  },
   *   ...
   * 
   * the default options when omitted are:
   *   overwrite: false
   *   base_path: '.' or if the src path contains '**' everything before that will be used ie:
   *                  if the src = 'src/**\/*.js' then the base_path will be 'src/'.
   */
  grunt.registerMultiTask('overlay', 'Copy files over directories', function () {
    var src_path = this.data.src;
        dest_path = this.data.dest,
        options = this.options({
          overwrite: false,
          base_path: null
        });

    // If only 1 src expression is used see if we can determine a base path using '/**/'
    if (src_path != null && options.base_path == null) {
      var v = src_path.split(path.sep + '**' + path.sep);
      if (v.length === 1 && path.sep !== '/') { // because windows can be either '/' or '\'
        v = src_path.split('/**/');
      }
      options.base_path = v.length === 1 ? '' : v[0]; 
    }

    // Create the dictionary of source to destination file entries
    this.files[0].src.map(function (file_path) {
      return {
        src: file_path,
        dest: path.normalize(file_path.replace(
            new RegExp('^' + options.base_path), dest_path + path.sep))
      }
    }).forEach(function (file_map_entry) {
      if (file_map_entry.src !== file_map_entry.dest) { // safety check
        overlay(file_map_entry.src, file_map_entry.dest, options.overwrite);
      }
    });
  });

  /**
   * The grunt task helper to provide overlay copying operations.
   * 
   * @param {string} src The source file path to copy to the destination.
   * @param {string} dest The destination to copy the file.
   * @param {boolean} overwrite Overwrite the file if it already exists.
   **/
  function overlay(src, dest, overwrite) {
    if (fs.existsSync(src)) {
      grunt.verbose.write('"' + src + '" -> "' + dest + '" ');

      if (fs.existsSync(dest) && !overwrite) {
        grunt.verbose.ok('\t SUCCESS (NO - OVERWRITE)');
        return true;
      }

      dest.split(path.sep).slice(0, -1).map(function(pathSegment, index, fullPath) {
        currentPath = fullPath.slice(0, 1 + index).join(path.sep);
        if (fs.existsSync(currentPath)) {
          if (fs.statSync(currentPath).isDirectory()) {
            return;
          }
          fs.unlinkSync(currentPath);
        }
        fs.mkdirSync(currentPath);
      });

      if (!fs.statSync(src).isDirectory) {
        fs.writeFileSync(dest, fs.readFileSync(src));
      }

      grunt.verbose.ok('\tSUCCESS');

      return true;
    } else {
      grunt.log.error('Unable to find source file :"' + src + '".');
      return false;
    }
  }
}
