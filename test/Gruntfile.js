module.exports = function (grunt) {
  grunt.initConfig({
    overlay: {
      simple: {
        src: 'fixtures/*',
        dest: 'output'
      },
      base: {
        src: 'fixtures/*',
        dest: 'output',
        options: {
          base_path: 'fixtures'
        }
      },
      glob: {
        src: 'fixtures/**/*.filter',
        dest: 'output',
      },
      overwrite: {
        src: 'fixtures/**/*.filter',
        dest: 'output',
        options: {
          overwrite: true
        }
      },
      badoverwrite: {
        src: 'fixtures/*',
        dest: 'fixtures',
        options: {
          overwrite: true
        }
      }
    }
  });

  grunt.loadTasks('../tasks');
  grunt.registerTask('default', ['overlay']);
};
